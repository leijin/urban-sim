#### Summary

(Summarize the bug encountered concisely)



#### Current bug behavior:

(What actually happens)



#### Expected correct behavior

(What you should see instead)



#### Relevant logs and/or screenshots

(Paste any relevant logs - please use code blocks (```) to format console output, logs, and code.)



#### Environment

App version:

OS and version:

Browser and its version:

Node version:

Branch and commit:




#### Steps to reproduce (optional)

(How one can reproduce the issue - nice to have but this is very important)



#### Possible fixes (optional)

(If you can, link to the line of code that might be responsible for the problem)


#### Actions (optional)

/label ~bug ~reproduced ~needs-investigation
/cc @ljin
/assign 


#### Other comment (optional)